import { useState } from "react";
import "./App.css";
var data = require("./drug1.json");

export default function App() {
  const [value, setValue] = useState("");

  const onChange = (event) => {
    setValue(event.target.value);
  };

  const onSearch = (searchTerm) => {
    setValue(searchTerm);
    // our api to fetch the search result
    console.log("search ", searchTerm);
  };

  return (
    <div className="App">
      <h1>Search</h1>

      <div className="search-container">
        <div className="search-inner">
          <input type="text" value={value} onChange={onChange}  />
          <button onClick={() => onSearch(value)}> Search </button>
        </div>
        <div className="dropdown">
          {data
            .filter((item) => {
              const searchTerm = value.toLowerCase();
              const label = item.label.toLowerCase();

              return (
                searchTerm &&
                label.startsWith(searchTerm) &&
                label !== searchTerm
              );
            })
          
            .map((item) => (
              <div
                onClick={() => onSearch(item.label)}
                className="dropdown-row"
                key={item.label}
              >
                {item.label}
                {item.type}
                
              </div>

            ))}

            
        </div>
      </div>

      <div>
        
      </div>
    </div>
  );
}
